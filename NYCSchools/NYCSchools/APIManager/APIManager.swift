//
//  NetworkManager.swift
//  NYCSchools
//
//  Created by Raju Ramalingam on 19/03/23.
//

import Foundation
import Alamofire

class APIManager: NSObject {
    static let shared = APIManager()
    
    override init() {}
    
    func getSchoolList(
        urlString: String,
        parameters: [String: Any] = [:],
        headers: HTTPHeaders,
        completion: @escaping ([SchoohInfo]?, Error?) -> Void
    ) {
        AF.request(urlString, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: [:])
            .validate()
            .responseDecodable(of: [SchoohInfo].self) { response in
                switch response.result {
                case .success(let schoolInfo):
                    completion(schoolInfo, nil)
                case .failure(let error):
                    completion(nil, error)
                }
            }
    }
        
        func schoolInfo(
            urlString: String,
            parameters: [String: Any] = [:],
            headers: HTTPHeaders,
            completion: @escaping ([SchoolSatInfo]?, Error?) -> Void
        ) {
            AF.request(urlString, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: [:])
                .validate()
                .responseDecodable(of: [SchoolSatInfo].self) { response in
                    switch response.result {
                    case .success(let schoolInfo):
                        completion(schoolInfo, nil)
                    case .failure(let error):
                        completion(nil, error)
                    }
                }
        }
        
}
