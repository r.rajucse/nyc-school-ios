//
//  SchoolListViewModel.swift
//  NYCSchools
//
//  Created by Raju Ramalingam on 19/03/23.
//

import Foundation
import Alamofire

class SchoolListViewModel {
    init() {
    }
    
    func getSchoolList(limit: Int, offset: Int, completion: @escaping (([SchoohInfo]?) -> Void)) {
        let formUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$limit=\(limit)&$offset=\(offset)"
        APIManager.shared.getSchoolList(urlString: formUrl, headers: .default) { schoolList, error in
            guard let list = schoolList else {
                completion(nil)
                return
            }
            completion(list)
        }
    }
}
