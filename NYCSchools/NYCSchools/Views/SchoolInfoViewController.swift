//
//  SchoolInfoViewController.swift
//  NYCSchools
//
//  Created by Raju Ramalingam on 19/03/23.
//

import UIKit
import Alamofire

class SchoolInfoViewController: UIViewController {
    
    private var reachability: NetworkReachabilityManager!
    private var schoolInfoViewModel: SchoolInfoViewModel?
    var schoolSatArray: [SchoolSatInfo] = []
    var dbn: String?
    
    @IBOutlet weak var satTestTakers: UILabel!
    @IBOutlet weak var criticalAvgScore: UILabel!
    @IBOutlet weak var mathAvgScore: UILabel!
    @IBOutlet weak var satWritingAvgScore: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialUISetup()
        initialSetup()
    }
    
    private func initialSetup() {
        self.schoolInfoViewModel = SchoolInfoViewModel()
        self.getSchoolInfoList()
        reachability = NetworkReachabilityManager.default
        monitorReachability()
    }
    
    private func initialUISetup() {
        self.stackView.layer.cornerRadius = 7.0
    }
    
    // MARK:- Reachability
    private func monitorReachability() {
        reachability.startListening { status in
            switch status {
            case .notReachable: break
            case .reachable(let type):
                if type == .cellular {
                    
                } else {
                    
                }
            case .unknown: break
            }
            print("Reachability Status Changed: \(status)")
        }
    }
    
    /// Get School Info Api call
    private func getSchoolInfoList() {
        guard let dbnName = dbn else {
            return
        }
        schoolInfoViewModel?.getSchoolSatInfo(dbn: dbnName) { schoolInfoList in
            print(schoolInfoList ?? [])
            guard let infoList = schoolInfoList, !infoList.isEmpty else {
                return
            }
            self.schoolSatArray.append(contentsOf: infoList)
            self.navigationItem.title =  self.schoolSatArray.first?.schoolName
            self.satTestTakers.text = "Sat Tes Takers: " + (self.schoolSatArray.first?.numOfSatTestTakers ?? "")
            self.criticalAvgScore.text = "Critical Reading Avg Score: " +  (self.schoolSatArray.first?.satCriticalReadingAvgScore ?? "")
            self.mathAvgScore.text = "Math Avg Score: " +  (self.schoolSatArray.first?.satMathAvgScore ?? "")
            self.satWritingAvgScore.text = "Writing Avg Score: " +  (self.schoolSatArray.first?.satWritingAvgScore ?? "")
        }
    }
    
    @IBAction func actionClose(_ sender: Any) {
        self.dismiss(animated: true)
    }
}
