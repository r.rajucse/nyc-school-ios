//
//  ViewController.swift
//  NYCSchools
//
//  Created by Raju Ramalingam on 19/03/23.
//

import UIKit
import Alamofire

class SchoolListViewController: UITableViewController {
    private var reachability: NetworkReachabilityManager!
    private var schoolListViewModel: SchoolListViewModel?
    var offset = 0
    var limit = 10
    var loadMoreInProgress = false
    var schoolListArray: [SchoohInfo] = []
    
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelLoadMore: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    private func initialSetup() {
        self.schoolListViewModel = SchoolListViewModel()
        self.getSchoolList()
        reachability = NetworkReachabilityManager.default
        monitorReachability()
    }
    
    // MARK:- Reachability
    private func monitorReachability() {
        reachability.startListening { status in
            switch status {
            case .notReachable: break
            case .reachable(let type):
                if type == .cellular {
                    
                } else {
                    
                }
            case .unknown: break
            }
            print("Reachability Status Changed: \(status)")
        }
    }
    
    /// Get School List Api call
    private func getSchoolList() {
        schoolListViewModel?.getSchoolList(limit: limit, offset: offset) { schoolList in
            print(schoolList ?? [])
            guard let list1 = schoolList, !list1.isEmpty else {
                self.loaderView.isHidden = true
                return
            }
            DispatchQueue.main.async {
                self.loadMoreSchools(isLoadMore: true)
                self.schoolListArray.append(contentsOf: list1)
                self.tableView.reloadData()
                self.tableView.scrollToRow(at: IndexPath(row: self.schoolListArray.count - 1, section: 0), at: .bottom, animated: true)
            }
        }
    }
    
    private func increseOffset() {
        self.loadMoreInProgress = true
        offset += 50
    }
    
    private func loadMoreSchools(isLoadMore: Bool) {
        self.loaderView.isHidden = false
        self.labelLoadMore.text = isLoadMore ? "Load More": "Downloading Schools..."
        self.activityIndicator.isHidden = isLoadMore
    }
    
    private func loadMoreSchools(){
        self.increseOffset()
        self.getSchoolList()
    }
    
    private func getDbnForSchollSatInfo(dbn: String) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let schoolInfoViewController = storyBoard.instantiateViewController(withIdentifier: "SchoolInfo") as? SchoolInfoViewController else {
            return
        }
        schoolInfoViewController.dbn = dbn
        let navigationController = UINavigationController.init(rootViewController: schoolInfoViewController)
        navigationController.modalPresentationStyle = .fullScreen
        navigationController.navigationBar.isHidden = false
        self.present(navigationController, animated: true)
    }
}

extension SchoolListViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        schoolListArray.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let listCell = tableView.dequeueReusableCell(withIdentifier: "SchoolListCell") as? SchoolListCell else {
            return UITableViewCell()
        }
        listCell.textLabel?.text = schoolListArray[indexPath.row].schoolName
        return listCell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        getDbnForSchollSatInfo(dbn: schoolListArray[indexPath.row].dbn ?? "")
    }
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print("scrollViewWillBeginDragging")
        loadMoreInProgress = false
    }

    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating")
    }
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        print("scrollViewDidEndDragging")
        if ((tableView.contentOffset.y + tableView.frame.size.height) >= tableView.contentSize.height)
        {
            if !loadMoreInProgress {
                loadMoreInProgress = true
                self.loadMoreSchools(isLoadMore: false)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    self.loadMoreSchools()
                }
            }
        }
    }
}
