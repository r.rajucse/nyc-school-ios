//
//  SchoolInfoViewModel.swift
//  NYCSchools
//
//  Created by Raju Ramalingam on 19/03/23.
//

import Foundation

class SchoolInfoViewModel {
    init() { }
    func getSchoolSatInfo(dbn: String, completion: @escaping (([SchoolSatInfo]?) -> Void)) {
        let formUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(dbn)"
        APIManager.shared.schoolInfo(urlString: formUrl, headers: .default) { schoolSatInfo, error in
            guard let list = schoolSatInfo else {
                completion(nil)
                return
            }
            completion(list)
        }
    }
}
